Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #Home
  root 'articles#index'

  #About
  get 'about', to: 'pages#about'

  #Articles
  resources :articles

  #drafts
  get '/drafts', to: 'articles#drafts'

  #Tags
  get '/tags', to: 'tags#index'
  post '/tags', to: 'tags#create'
  get '/tags/new', to: 'tags#new'
  get 'tags/:id/edit', to: 'tags#edit'
  get '/tags/:taggy', to: 'articles#index', as: 'tag', param: :taggy
  patch 'tags/:id', to: 'tags#update'
  put 'tags/:id', to: 'tags#update'
  delete 'tags/:id', to: 'tags#destroy'





  post 'publish_draft', to: 'articles#publish_draft'

  #En vez de usar resources, también puede ser:
  #get '/articles', to 'articles#index'
  #get '/articles/:id', to 'articles#show', as 'article'
  #y así...

  devise_for :users, controllers: { registrations: "registrations"}

  #ckeditor stuff
  mount Ckeditor::Engine => '/ckeditor'

end
