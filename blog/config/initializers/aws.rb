require 'aws-sdk'

Aws.config.update(
    {
        region: ENV['S3_REGION'],
        credentials: Aws::Credentials.new(ENV['S3_ID'], ENV['S3_PASS']),
    }
)

#paperclip fix
#Aws::VERSION =  Gem.loaded_specs["aws-sdk"].version
