class CreateArticlesTags < ActiveRecord::Migration[5.0]
  def change
    create_table :articles_tags, :id => false do |t|
      t.column :article_id, :integer
      t.column :tag_id, :integer
    end
  end
end
