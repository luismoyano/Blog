class ArticlesController < ApplicationController

before_action :get_article_by_id, only: [:edit, :update, :show, :destroy, :publish_draft]
before_action :require_same_user, only: [:edit, :update, :destroy]
before_action :require_user, only: [:new]
  def new
    @article = Article.new
  end


  def create
    @article = Article.new(article_params)
    @article.user = current_user

    #Guardo los tags
    params[:article][:tags].each{ |tag_id|
      if tag_id != ""
        @tag = Tag.find_by_id(tag_id.to_i)
        @article.tags << @tag
      end
    }

    if @article.save
      #Notificamos éxito
      flash[:notice] = "Article successfully created"
      redirect_to article_path(@article)
    else
      render :new
    end
  end

  def edit

  end

  def update
    if @article.update(article_params)
      flash[:notice] = "Article Successfully updated"
      redirect_to article_path(@article)
    else
      render :edit
    end
  end

def index
  if !params[:taggy].blank?
    get_articles_by_tag(params[:taggy])
  else
    get_all_articles
  end

end

def drafts
  get_all_drafts
  render :index
end

def publish_draft

  if @article.draft?
    @article.draft = false;
    @article.save!
  end

  redirect_to article_path(@article)

end

def show
end


def destroy
  if @article.destroy
      flash[:notice] = "Article successfully deleted"
      redirect_to articles_path
    end
  end

  private

  def get_article_by_id
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :content, :draft, :header_img)
  end

  def require_same_user
    if current_user.blank? || @article.user != current_user
      flash[:danger] = "You can only modify your own stuff"
      redirect_to root_path
    end
  end

  def require_user
    if current_user.blank?
      flash[:danger] = "You can only modify your own stuff"
      redirect_to root_path
    end
  end

  def paginate_articles
    if !@articles.blank?
      @articles = @articles.paginate(:page =>params[:page]).order('created_at DESC')
    end

  end

  def fetch_html_js
    #Can be fetched with html or js
    respond_to do |format|
      format.html
      format.js
    end
  end

  def get_all_articles
    @articles = Article.where(draft: false)

    paginate_articles
    fetch_html_js
  end

  def get_articles_by_tag(tag)
    @tag = Tag.find_by_taggy(tag)
    if(!@tag.blank?)
      @articles = @tag.articles.where(draft: false)
    end

    paginate_articles
    fetch_html_js
  end

  def get_all_drafts
    @articles = Article.where(draft: true)

    paginate_articles
    fetch_html_js
  end

end
