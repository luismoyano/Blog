class RegistrationsController < Devise::RegistrationsController

  before_action :one_user_registered?, only: [:new, :create]

  protected

  def one_user_registered?
    if ((User.count == ALLOWED_USERS) & (user_signed_in?))
      redirect_to root_path
    elsif User.count == ALLOWED_USERS
      redirect_to new_user_session_path
    end
  end

  private

  ALLOWED_USERS = 1.freeze

end
