class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :check_manteinance_mode

  def require_admin
    if current_user.blank? || !current_user.admin?
      redirect_to root_path
    end
  end


  private

  def check_manteinance_mode
    if ActiveModel::Type::Boolean.new.cast(ENV['MANTEINANCE_MODE'])
      #p ENV['MANTEINANCE_MODE']
      render file: 'public/manteinance.html.erb'
    end
  end

end
