class TagsController < ApplicationController

  before_action :get_tag_by_id, only: [:edit, :update, :show, :destroy]
  before_action :require_admin, only: [:new, :edit, :update, :destroy]


  def new
    @tag = Tag.new
  end


  def create
    @tag = Tag.new(tag_params)
    if @tag.save
      #Notificamos éxito
      flash[:notice] = "tag successfully created"
      redirect_to tag_path(@tag)
    else
      render :new
    end
  end

  def edit

  end

  def update
    if @tag.update(tag_params)
      flash[:notice] = "tag Successfully updated"
      redirect_to tag_path(@tag)
    else
      render :edit
    end
  end

  def index

    @tags = Tag.all
    @tags = @tags.paginate(:page =>params[:page]).order('created_at DESC')


    #Can be fetched with html or js
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
  end

  def destroy
    if @tag.destroy
      flash[:notice] = "tag successfully deleted"
      redirect_to tags_path
    end

  end

  private

  def get_tag_by_id
    @tag = Tag.find(params[:id])
  end

  def tag_params
    params.require(:tag).permit(:taggy)
  end

end
