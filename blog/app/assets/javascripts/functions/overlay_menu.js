function openMenu() {

    var overlay = document.getElementById('menu-overlay');

    overlay.style.display = 'block';
    overlay.style.opacity = '1';


    //console.log("Abro el menú");
}

function closeMenu() {
    //console.log("Cierro el menú");
    var overlay = document.getElementById('menu-overlay');

    overlay.style.opacity = "0";

    overlay.addEventListener("transitionend", function(event){
      overlay.style.display = 'none';
    }, false);
}
