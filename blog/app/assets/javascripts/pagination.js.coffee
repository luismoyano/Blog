jQuery ->

  if $('#infinite-scrolling').size() > 0
    $('.pagination').hide()
    loading_posts = false

  $('#load_more_posts').show().click ->
    unless loading_posts
      loading_posts = true
      more_posts_url = $('.pagination .next_page').attr('href')
      if more_posts_url
        $this = $(this)
        $this.html('<p>Loading...</p>').addClass('disabled')

        $.getScript more_posts_url, ->
          $this.text('More posts').removeClass('disabled') if $this
          loading_posts = false
      else
        $('#load_more_posts').hide()
    return
  return
