if (typeof(CKEDITOR) != 'undefined') {
  CKEDITOR.editorConfig = function(config) {
    config.language = 'en';
    config.width = '100%';
    config.height = '70%';
    config.filebrowserBrowseUrl = "/ckeditor/attachment_files";
    config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";
    config.filebrowserImageBrowseUrl = "/ckeditor/pictures";
    config.filebrowserImageUploadUrl = "/ckeditor/pictures";
    config.filebrowserUploadUrl = "/ckeditor/attachment_files";

    config.fontSize_sizes= '1/10vh;1.5/15vh;2/20vh;2.5/25vh;3/30vh;3.5/35vh;4/40vh;4.5/45vh;5/50vh;5.5/55vh;6/60vh;6.5/65vh;';

    config.toolbar_Pure = [
      '/', {
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']
      }, {
        name: 'paragraph',
        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
      }, {
        name: 'links',
        items: ['Link', 'Unlink']
      }, '/', {
        name: 'styles',
        items: ['Styles', 'Format', 'Font', 'FontSize']
      }, {
        name: 'colors',
        items: ['TextColor', 'BGColor']
      }, {
        name: 'insert',
        items: ['Image', 'Table', 'HorizontalRule', 'PageBreak']
      }
    ];
    config.toolbar = 'Pure';
    return true;
  };
}
