class Tag < ApplicationRecord
  validates_presence_of :taggy
  validates_uniqueness_of :taggy

  validates :taggy, format: { without: /\s/ }

  has_and_belongs_to_many :articles

end
