class Ckeditor::AttachmentFile < Ckeditor::Asset
  has_attached_file :data,
                    url: "/ckeditor_assets/pictures/:id/:style_:basename.:extension",
                    path: "/ckeditor_assets/pictures/:id/:style_:basename.:extension",
                    :styles => { :content => '800>', :thumb => '118x100#' },
                      :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/aws.yml",
                    :bucket => 'shipmetrics'

  validates_attachment_presence :data
  validates_attachment_size :data, less_than: 100.megabytes
  do_not_validate_attachment_file_type :data

  def url_thumb
    @url_thumb ||= Ckeditor::Utils.filethumb(filename)
  end

  def url_content
      url(:content)
    end
end
