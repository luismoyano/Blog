class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable,
         :timeoutable

  #email override
  validates_uniqueness_of :email
  validates_presence_of :email
  validates_format_of :email, with: Devise.email_regexp

  #Author
  #validates :author, presence: true

  has_many :articles
end
