class Article < ApplicationRecord
  validates :title, presence: true, length: {minimum: 3, maximum: 140}
  validates :content, presence: true
  validates :user_id, presence: true

  belongs_to :user
  has_and_belongs_to_many :tags

  #Header image
  has_attached_file :header_img, styles: {large: "1500x1500>", medium: "600x600>", thumb: "150x150#"}, default_url: 'https://s3.eu-west-2.amazonaws.com/luismoyanoblog/articles/header_imgs/medium/missing.png'
  validates_attachment_content_type :header_img, content_type: /\Aimage\/.*\Z/


  TAGS_PREVIEW_LIMIT = 2.freeze

  def timestamp
    created_at.strftime('%d %B %Y %H:%M:%S')
  end

  def short_timestamp
    created_at.strftime('%m/%Y')
  end

end
