require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    @article = Article.new(title: "Quesos, cosas, casas", content: "Cosas");
  end


  test "Valid article" do
    assert@article.valid?
  end


  # Called after every test method runs. Can be used to tear
  # down fixture information.
  def teardown
    # Do nothing
  end
end